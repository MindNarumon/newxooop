import java.util.Scanner;
public class Game {
	private Board board;
	private Player x;
	private Player o;
	private int row,col;
	
	Game(){
		o = new Player('O');
		x = new Player('X');
		board = new Board(x,o);
	}
	public void play() {
		showWelcome();
		showTable();
		while(true) {
		showTurn();
		input();
		setTable();
		if(board.isEnd()) {
			showTable();
			showResult();
			break;
		}
		if(board.getCount()>=9) {
			showTable();
			showResult();
			break;
		}
		getPlayer();
		showTable();
		}
	}
	
	private void showWelcome() {
		System.out.println("              Welcome to Game XO");
	}

	private void showTable() {
		
		System.out.println("");
	
		System.out.printf("\t\t %c | %c | %c\n",board.Table[0][0],board.Table[0][1],board.Table[0][2]);
		System.out.print ("\t\t---+---+---\n");
		System.out.printf("\t\t %c | %c | %c\n",board.Table[1][0],board.Table[1][1],board.Table[1][2]);
		System.out.print ("\t\t---+---+---\n");
		System.out.printf("\t\t %c | %c | %c\n",board.Table[2][0],board.Table[2][1],board.Table[2][2]);
		System.out.println();
		
	}
	private void showTurn() {
		System.out.print("Player "+ board.getPlayer() +", please enter the number [your] "+ board.getCurrent().getName()+" :");
	}
	private void input() {
		Scanner kb= new Scanner(System.in);
		int RC = kb.nextInt();
		row =board.getRow(RC);
		col =board.getColumn(RC);
	}
	private void setTable() {
		board.Table[row][col] = board.getCurrent().getName();

	}
	public void showResult() {
		if(board.isEnd()) {
			System.out.println("Congratuiations!!, Player " +board.getPlayer()+" ["  +board.getCurrent().getName()+  "] "+", YOU ARE THE WINNER!");
		}else {
			System.out.printf("\tHow boring, it is a draw");
		}
	}
	public void getPlayer() {
		board.swichTurn();
	}
	
}
